import java.util.Random;
import java.util.Arrays;
import java.util.ArrayList;

public class Deck {

    ArrayList<Character> cards = new ArrayList<Character>();

    Deck(int card_no) {

        char[] choices = {'0', '1'};

        Random r = new Random();

        for(int i = 0; i < card_no; i++){
            cards.add(choices[r.nextInt(2)]);
        }


    }

    public boolean possible_move() {

        boolean can_play = false;

        for(int i = 0; i < cards.size(); i++) {

            if(cards.get(i) == '1') {
                can_play = true;
            }

        }

        return can_play;

    }

    public boolean has_won() {

        boolean win = true;

        for(int i = 0; i < cards.size(); i++) {

            if(cards.get(i) != '.') {
                win = false;
            }

        }

        return win;

    }

    public void flip(int card_index) {

        if(card_index < 0){
            return;
        }

        char card = cards.get(card_index);

        if(card == '1'){
            cards.set(card_index, '0');
        } else if (card == '0') {
            cards.set(card_index, '1');
        }

    }

    public void remove(int card_index){

        char card = cards.get(card_index);

        // System.out.println(card);

        if(card == '1') {

            cards.set(card_index, '.');
            try {
                flip(card_index - 1);
            } catch (Exception e) {}

            try {
                flip(card_index + 1);
            } catch (Exception e) {}

        }


    }

    private void set_cards(ArrayList<Character> l) {
        cards = l;
    }

    public static Deck from_arraylist(ArrayList<Character> lst) {
        Deck result = new Deck(lst.size());
        result.set_cards(lst);
        return result;
    }

    public void show(){

        for(int i = 0; i < cards.size(); i++) {

            System.out.print(cards.get(i) + "  ");

        }
        System.out.println("");

    }

}