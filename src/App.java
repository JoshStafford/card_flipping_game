import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class App {

    public static void main(String[] args){

        List<Character> __new_d = Arrays.asList('0','0','1','0','1','1','0','1',
        '1','1','0','1','0','0','1','0','0','1','0','0','0');
        ArrayList<Character> new_d = new ArrayList<Character>(__new_d);
        
        Deck d = Deck.from_arraylist(new_d);
        Card_Flipping game = new Card_Flipping();
        game.play(d);

    }

}