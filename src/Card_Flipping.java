import java.util.Scanner;

// Can only draw face-up cards
// When card drawn, adjacent cards must be flipped

public class Card_Flipping {

    Scanner keyboard = new Scanner(System.in);

    Card_Flipping(){}

    public void play(Deck d) {

        int card_choice;

        while(!d.has_won() && d.possible_move()) {

            d.show();

            System.out.println("Pick a card...");
            card_choice = keyboard.nextInt();

            while(card_choice >= d.cards.size()){
                System.out.println("\nPick a card...");
                card_choice = keyboard.nextInt();
            }

            d.remove(card_choice);


        }

        if(d.has_won()){
            System.out.println("You win!");
        } else {
            System.out.println("You lose.");
        }

    }

}